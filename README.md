# `copier-python-django-meta`

Generate the `copier-python-django` template and then a project:

```shell
copier https://gitlab.com/copier-meta-templates-example/copier-python-django-meta copier-python-django
copier copier-python-django djangoproj
```
